FROM debian:12-slim

LABEL org.opencontains.image.authors="TurboTurtle devs" \
      version="debian12-1.0" \
      description="A debian container with QT, OpenCV and NDI bundled. This is mostly used for TurboTurtle's CI/CD and may not be adapted for other workflows"

# For apt commands
ARG DEBIAN_FRONTEND=noninteractive

# Install QT/OpenCV & curl then link opencv2
RUN apt update \
     && apt install -y --no-install-suggests --no-install-recommends \
        build-essential \
        cmake \
        libopencv-dev \
        qt6-multimedia-dev \
        qt6-tools-dev \
        curl \
        ca-certificates \
        libquazip1-qt6-dev \
     && apt autoremove --purge \
     && rm -rf /usr/share/doc/* \
               /usr/share/publicsuffix/* \
               /var/lib/dpkg \
               /var/log/* \
               /var/cache/* \
               /usr/share/zsh \
     && ln -s /usr/include/opencv4/opencv2/ /usr/include/opencv2

# Use bash for NDI
SHELL ["/bin/bash", "-c"]

# Install NDI, then clear
RUN set -e \
    && INSTALLER_NAME="Install_NDI_SDK_v5_Linux" \
    && INSTALLER_FILE="$INSTALLER_NAME.tar.gz" \
    && pushd /tmp \
    && curl -L -o $INSTALLER_FILE https://downloads.ndi.tv/SDK/NDI_SDK_Linux/$INSTALLER_FILE -f --retry 5 \
    && tar -xf $INSTALLER_FILE \
    && yes | PAGER="cat" sh $INSTALLER_NAME.sh \
    && rm -rf ndisdk \
    && mv "NDI SDK for Linux" ndisdk \
    && cp -P ndisdk/lib/x86_64-linux-gnu/* /usr/local/lib/ \
    && ldconfig \
    && cp -P ndisdk/include/* /usr/include/ \
    && rm -rf ndisdk \
    && popd \
    && rm -rf /tmp/* /var/tmp/* /etc/ssl/* /usr/share/ca-certificates/mozilla/* /usr/share/doc/* /usr/lib/ssl/*

WORKDIR /root
